<?php

namespace Drupal\ckeditor_copyformatting\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "CKEditor Copy Formatting" plugin.
 *
 * @CKEditorPlugin(
 *   id = "copyformatting",
 *   label = @Translation("CKEditor Copy Formatting"),
 *   module = "ckeditor_copyfomratting"
 * )
 */
class CkeditorCopyFormatting extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->getPluginPath() . '/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'CopyFormatting' => [
        'label' => $this->t('Copy formatting'),
        'image' => $this->getPluginPath() . '/icons/copyformatting.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * Return ckeditor copyformatting plugin path relative to drupal root.
   *
   * @return string
   *   Relative path to the ckeditor plugin folder
   */
  private function getPluginPath() {
    return 'libraries/copyformatting';
  }

}
